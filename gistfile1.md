Steps
======
- Build tools:

        sudo yum groupinstall "Development Tools"
        sudo yum install openssl openssl-devel
        sudo yum install git-core

- Dependencies:

        yum install -y gcc-c++ patch readline readline-devel zlib zlib-devel \
        libyaml-devel libffi-devel openssl-devel make bzip2 autoconf automake libtool bison iconv-devel

- Install RVM

        bash -s stable < <(curl -s https://raw.github.com/wayneeseguin/rvm/master/binscripts/rvm-installer)

- Install Ruby 

        rvm install 1.9.2

- Install Rails, Bundler (problem was here)

        gem install bundler --no-rdoc --no-ri
        gem install rails --no-rdoc --no-ri

- Sqlite3

        sudo yum install sqlite3-dev
        
- MySql

        sudo yum install mysql
        sudo mkdir /var/lib/mysql
        sudo yum install mysql-server
        nano /etc/my.cnf
        sudo /etc/init.d/mysqld start
        mysqladmin -u root password '123456789'
        sudo chkconfig mysqld on

- Node JS 

        wget http://nodejs.org/dist/v0.6.15/node-v0.6.15.tar.gz
        tar -xf node-v0.6.15.tar.gz 
        cd node-v0.6.15
        ./configure
        sudo make install
        
- MongoDB

        curl http://downloads.mongodb.org/linux/mongodb-linux-x86_64-2.4.5.tgz > mongodb.tgz
        tar -zxvf mongodb.tgz
        
        or
        
        http://docs.mongodb.org/ecosystem/tutorial/install-mongodb-on-amazon-ec2/

Shortcut way to try out:
    
    sudo yum localinstall --nogpgcheck http://nodejs.tchol.org/repocfg/fedora/nodejs-stable-release.noarch.rpm
    sudo yum install nodejs

Problem #1:zlib
======
Symptoms
------

    [ec2-user@domU-12-31-38-04-80-4D zlib]$ gem install rails --include-dependencies
        ERROR:  Loading command: install (LoadError)
            no such file to load -- zlib
        ERROR:  While executing gem ... (NameError)
            uninitialized constant Gem::Commands::InstallCommand

Solution
------
Originally taken from [here](http://stackoverflow.com/questions/4879744/ruby-on-ec2-install-problem-with-zlib)

    cd /home/ec2-user/.rvm/src/ruby-1.9.2-p320/ext/zlib
    ruby extconf.rb --with-zlib-include=/usr/include --width-zlib-lib=/usr/lib
    make 
    sudo make install
    gem install rails --include-dependencies

Problem #2: openssl
=======
Originally taken from [here](http://railsforum.com/viewtopic.php?id=39587)

    cd /home/ec2-user/.rvm/src/ruby-1.9.2-p320/ext/openssl
    ruby extconf.rb
    make
    make install